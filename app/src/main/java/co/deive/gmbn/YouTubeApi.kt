package co.deive.gmbn

import android.os.Parcelable
import com.fatboyindustrial.gsonjodatime.Converters
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.singleton
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Kodein module for the YouTubeApi.
 */
class APIModule {

    val module = Kodein.Module {
        bind<YouTubeApi>() with singleton {
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(
                    Converters.registerDateTime(GsonBuilder()).create()))
//                    GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create()))
                .baseUrl(YouTubeApi.baseURL)
                .build()
                .create(YouTubeApi::class.java)
        }
    }

}

/**
 * Main YouTube API for the GMBN App.
 * Defines API methods for listing playlists from a channel, videos in a playlist and getting video details.
 * TODO: Use part query parameter to reduce response size.
 */
interface YouTubeApi {

    companion object {
        const val baseURL = "https://www.googleapis.com/youtube/v3/"
        const val key = "AIzaSyCpmGSzMcabkGqndzQ4330kRu4sDrg3xNk"
    }

    // https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId=UC6juisijUAHcJLt23nk-qOQ
    @GET("playlists")
    fun playlistForChannel(
        @Query("channelId") channelId: String = "UC_A--fhX5gea0i4UtpD99Gg",
        @Query("maxResults") maxResults: Int = 5,
        @Query("pageToken") pageToken: String? = null,
        @Query("part") part: String = "snippet",
        @Query("key") key: String = YouTubeApi.key
    ): Call<PlaylistResponse>

    // https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=PLXWBBaEdFtbItV6NyAbdhfMFUA-Z9SOQL
    @GET("playlistItems")
    fun itemsForPlaylist(
        @Query("playlistId") playlistId: String,
        @Query("maxResults") maxResults: Int = 5,
        @Query("pageToken") pageToken: String? = null,
        @Query("part") part: String = "snippet",
        @Query("key") key: String = YouTubeApi.key
    ): Call<PlaylistItemResponse>

    // https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&videoId=bNDVh5DjewU
    @GET("commentThreads")
    fun commentThreadsForVideo(
        @Query("videoId") videoId: String,
        @Query("maxResults") maxResults: Int = 5,
        @Query("pageToken") pageToken: String? = null,
        @Query("part") part: String = "snippet",
        @Query("key") key: String = YouTubeApi.key
    ): Call<CommentThreadResponse>

    // https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=bNDVh5DjewU
    @GET("videos")
    fun videoDetails(
        @Query("id") videoId: String,
        @Query("part") part: String = "contentDetails",
        @Query("key") key: String = YouTubeApi.key
    ): Call<VideoDetailsResponse>

}

data class PlaylistResponse(
    @SerializedName("kind") val kind: String,
    @SerializedName("etag") val etag: String,
    @SerializedName("nextPageToken") val nextPageToken: String?,
    @SerializedName("prevPageToken") val prevPageToken: String?,
    @SerializedName("pageInfo") val pageInfo: PageResponse,
    @SerializedName("items") val items: List<ItemResponse<PlaylistSnippetResponse>>
)

data class PlaylistItemResponse(
    @SerializedName("kind") val kind: String,
    @SerializedName("etag") val etag: String,
    @SerializedName("nextPageToken") val nextPageToken: String?,
    @SerializedName("prevPageToken") val prevPageToken: String?,
    @SerializedName("pageInfo") val pageInfo: PageResponse,
    @SerializedName("items") val items: List<ItemResponse<PlaylistItemSnippetResponse>>
)

data class CommentThreadResponse(
    @SerializedName("kind") val kind: String,
    @SerializedName("etag") val etag: String,
    @SerializedName("nextPageToken") val nextPageToken: String?,
    @SerializedName("prevPageToken") val prevPageToken: String?,
    @SerializedName("pageInfo") val pageInfo: PageResponse,
    @SerializedName("items") val items: List<ItemResponse<CommentThreadSnippetResponse>>
)

data class VideoDetailsResponse(
    @SerializedName("kind") val kind: String,
    @SerializedName("etag") val etag: String,
    @SerializedName("nextPageToken") val nextPageToken: String?,
    @SerializedName("prevPageToken") val prevPageToken: String?,
    @SerializedName("pageInfo") val pageInfo: PageResponse,
    @SerializedName("items") val items: List<ContentDetailsResponse<VideoContentDetailsResponse>>
)

data class PageResponse(
    @SerializedName("totalResults") val totalResults: Int,
    @SerializedName("resultsPerPage") val resultsPerPage: Int
)

@Parcelize
data class ItemResponse<SNIPPET: Parcelable>(
    @SerializedName("kind") val kind: String,
    @SerializedName("etag") val etag: String,
    @SerializedName("id") val id: String,
    @SerializedName("snippet") val snippet: SNIPPET
): Parcelable

@Parcelize
data class ContentDetailsResponse<SNIPPET: Parcelable>(
    @SerializedName("kind") val kind: String,
    @SerializedName("etag") val etag: String,
    @SerializedName("id") val id: String,
    @SerializedName("contentDetails") val snippet: SNIPPET
): Parcelable

@Parcelize
data class ThumbnailResponse(
    @SerializedName("url") val url: String,
    @SerializedName("width") val width: Int,
    @SerializedName("height") val height: Int
): Parcelable

@Parcelize
data class PlaylistSnippetResponse(
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
    @SerializedName("publishedAt") val publishedAt: DateTime,
    @SerializedName("thumbnails") val thumbnails: Map<String, ThumbnailResponse>
): Parcelable

@Parcelize
data class PlaylistItemSnippetResponse(
    @SerializedName("publishedAt") val publishedAt: DateTime,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
    @SerializedName("thumbnails") val thumbnails: Map<String, ThumbnailResponse>,
    @SerializedName("resourceId") val resourceId: ResourceResponse
): Parcelable

@Parcelize
data class ResourceResponse(
    @SerializedName("videoId") val videoId: String
): Parcelable

@Parcelize
data class CommentThreadSnippetResponse(
    @SerializedName("videoId") val videoId: String,
    @SerializedName("topLevelComment") val topLevelComment: ItemResponse<TopLevelCommentSnippetResponse>,
    @SerializedName("canReply") val canReply: Boolean,
    @SerializedName("totalReplyCount") val totalReplyCount: Int,
    @SerializedName("isPublic") val isPublic: Boolean
): Parcelable

@Parcelize
data class TopLevelCommentSnippetResponse(
    @SerializedName("authorDisplayName") val authorDisplayName: String,
    @SerializedName("authorProfileImageUrl") val authorProfileImageUrl: String,
    @SerializedName("authorChannelUrl") val authorChannelUrl: String,
    @SerializedName("authorChannelId") val authorChannelId: AuthorChannelIdResponse,
    @SerializedName("videoId") val videoId: String,
    @SerializedName("textDisplay") val textDisplay: String,
    @SerializedName("textOriginal") val textOriginal: String,
    @SerializedName("canRate") val canRate: Boolean,
    @SerializedName("viewerRating") val viewerRating: String,
    @SerializedName("likeCount") val likeCount: Int,
    @SerializedName("publishedAt") val publishedAt: DateTime,
    @SerializedName("updatedAt") val updatedAt: DateTime
): Parcelable

@Parcelize
data class AuthorChannelIdResponse(
    @SerializedName("value") val value: String
): Parcelable

@Parcelize
data class VideoContentDetailsResponse(
    @SerializedName("duration") val duration: String,
    @SerializedName("dimension") val dimension: String,
    @SerializedName("definition") val definition: String,
    @SerializedName("caption") val caption: String,
    @SerializedName("licensedContent") val licensedContent: Boolean,
    @SerializedName("projection") val projection: String
): Parcelable