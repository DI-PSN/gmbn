package co.deive.gmbn

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.paging.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import com.google.android.youtube.player.YouTubeIntents
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener
import kotlinx.android.synthetic.main.video_fragment.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISOPeriodFormat
import org.joda.time.format.PeriodFormatterBuilder

/**
 * Just contains a PlaylistFragment
 */
class VideoActivity : GmbnActivity() {

    companion object {
        internal const val ExtraPlaylistItem = "VideoActivity.PlaylistItem"

        fun createIntent(context: Context, playlistItem: ItemResponse<PlaylistItemSnippetResponse>): Intent {
            val intent = Intent(context, VideoActivity::class.java)
            intent.putExtra(ExtraPlaylistItem, playlistItem)
            return intent
        }
    }

    private val playlistItem by lazy { intent.getParcelableExtra<ItemResponse<PlaylistSnippetResponse>>(ExtraPlaylistItem) }

    override fun createFragment(): Fragment {
        val frag = VideoFragment()
        val args = Bundle()
        args.putParcelable(ExtraPlaylistItem, playlistItem)
        frag.arguments = args
        return frag
    }
}

/**
 * Shows a list of comment threads items from YouTube for a video.
 */
class VideoFragment : Fragment() {

    companion object {
        private const val TagVideoDescription = "VideoFragment.Description"
    }

    private val playlistItem by lazy { arguments!!.getParcelable<ItemResponse<PlaylistItemSnippetResponse>>(VideoActivity.ExtraPlaylistItem)!! }
    private val viewModel: VideoViewModel by viewModelProvider { VideoViewModel(it, playlistItem.snippet.resourceId.videoId) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.video_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        ViewCompat.setTransitionName(video_header, PlaylistActivity.HeroId)
        val imageOptions = DisplayImageOptions.Builder().build()
        ImageLoader.getInstance()
            .displayImage(playlistItem.snippet.getThumbnail(view.context).url, video_header, imageOptions, SimpleImageLoadingListener())
        video_content.onClick {
            context?.let { startActivity(YouTubeIntents.createPlayVideoIntent(it, playlistItem.snippet.resourceId.videoId)) }
        }

        video_title.text = playlistItem.snippet.title
        video_date.text = DateTimeFormat.mediumDateTime().print(playlistItem.snippet.publishedAt)
        video_description.text = playlistItem.snippet.description
        video_description.onClick {
            val frag = VideoDescriptionFragment.newInstance(playlistItem.snippet.description)
            ViewCompat.setTransitionName(video_description, VideoDescriptionFragment.HeroId)
            val fragmentTransaction = childFragmentManager.beginTransaction()
            fragmentTransaction.addSharedElement(video_description, VideoDescriptionFragment.HeroId)
            frag.show(fragmentTransaction, TagVideoDescription)
        }
        viewModel.duration.observe(this, Observer {
            video_duration.text = it
            video_duration.visibility = View.VISIBLE
        })

        val adapter = CommentThreadAdapter()
        comments.adapter = adapter
        viewModel.commentThreadList.observe(this, Observer {
            adapter.submitList(it)
        })
    }

}

class VideoDescriptionFragment : DialogFragment() {

    companion object {
        internal const val HeroId = "VideoDescription.Hero"
        private const val ExtraVideoDescription = "VideoDescriptionFragment.Description"

        fun newInstance(description: String): VideoDescriptionFragment {
            val frag = VideoDescriptionFragment()
            val args = Bundle()
            args.putString(ExtraVideoDescription, description)
            frag.arguments = args
            return frag
        }
    }

    private val videoDescription by lazy { arguments!!.getString(ExtraVideoDescription)!! }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.video_description_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val videoDescription = view.findViewById<TextView>(R.id.video_description)!!
        ViewCompat.setTransitionName(videoDescription, VideoDescriptionFragment.HeroId)
        videoDescription.text = this.videoDescription
    }

}

/**
 * The item view for a single comment thread.
 */
class CommentThreadViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.context.layoutInflater.inflate(R.layout.comment_thread_item, parent, false)) {

    private val itemAuthor: TextView by lazy { itemView.findViewById<TextView>(R.id.comment_author)  }
    private val itemText: TextView by lazy { itemView.findViewById<TextView>(R.id.comment_text)  }
    private val itemThumbnail: ImageView by lazy { itemView.findViewById<ImageView>(R.id.comment_author_thumbnail)  }

    private var item: ItemResponse<CommentThreadSnippetResponse>? = null

    fun bind(item: ItemResponse<CommentThreadSnippetResponse>) {
        this.item = item
        itemAuthor.text = item.snippet.topLevelComment.snippet.authorDisplayName
        itemText.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(item.snippet.topLevelComment.snippet.textDisplay, Html.FROM_HTML_MODE_COMPACT)
        } else {
            @Suppress("DEPRECATION")
            Html.fromHtml(item.snippet.topLevelComment.snippet.textDisplay)
        }
        val imageOptions = DisplayImageOptions.Builder()
            .showImageOnLoading(android.R.color.transparent)
            .cacheOnDisk(true)
            .build()
        ImageLoader.getInstance()
            .displayImage(item.snippet.topLevelComment.snippet.authorProfileImageUrl, itemThumbnail, imageOptions, SimpleImageLoadingListener())
    }

}

/**
 * Paged adapter for a comment thread.
 */
class CommentThreadAdapter : PagedListAdapter<ItemResponse<CommentThreadSnippetResponse>, CommentThreadViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CommentThreadViewHolder(parent)

    override fun onBindViewHolder(holder: CommentThreadViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    companion object {
        private val diffCallback: DiffUtil.ItemCallback<ItemResponse<CommentThreadSnippetResponse>> = object : DiffUtil.ItemCallback<ItemResponse<CommentThreadSnippetResponse>>() {
            override fun areItemsTheSame(old: ItemResponse<CommentThreadSnippetResponse>, new: ItemResponse<CommentThreadSnippetResponse>): Boolean {
                return old.id === new.id
            }

            override fun areContentsTheSame(old: ItemResponse<CommentThreadSnippetResponse>, new: ItemResponse<CommentThreadSnippetResponse>): Boolean {
                return old == new
            }
        }
    }
}

/**
 * Paged data source loading comment thread pages fom YouTube.
 */
class CommentThreadDataSource(kodein: Kodein, private val videoId: String) : PageKeyedDataSource<String, ItemResponse<CommentThreadSnippetResponse>>() {

    private val api: YouTubeApi = kodein.instance()

    override fun loadInitial(
        params: LoadInitialParams<String>,
        callback: LoadInitialCallback<String, ItemResponse<CommentThreadSnippetResponse>>) {
        val response = api.commentThreadsForVideo(videoId, maxResults = params.requestedLoadSize).execute()
        if (response.isSuccessful && response.body() != null) {
            val playlist = response.body()!!
            callback.onResult(playlist.items, 0, playlist.pageInfo.totalResults,
                playlist.prevPageToken, playlist.nextPageToken)
        }
        else {
            callback.onResult(emptyList(), 0, 0, null, null)
        }
    }

    override fun loadAfter(
        params: LoadParams<String>,
        callback: LoadCallback<String, ItemResponse<CommentThreadSnippetResponse>>) {
        val response = api.commentThreadsForVideo(videoId, pageToken = params.key, maxResults = params.requestedLoadSize).execute()
        if (response.isSuccessful && response.body() != null) {
            val playlist = response.body()!!
            callback.onResult(playlist.items, playlist.nextPageToken)
        }
        else {
            callback.onResult(emptyList(), null)
        }
    }

    override fun loadBefore(
        params: LoadParams<String>,
        callback: LoadCallback<String, ItemResponse<CommentThreadSnippetResponse>>) {
        val response = api.commentThreadsForVideo(videoId, pageToken = params.key, maxResults = params.requestedLoadSize).execute()
        if (response.isSuccessful && response.body() != null) {
            val playlist = response.body()!!
            callback.onResult(playlist.items, playlist.prevPageToken)
        }
        else {
            callback.onResult(emptyList(), null)
        }
    }

}

/**
 * Video view model to hold comment thread data.
 */
class VideoViewModel(kodein: Kodein, videoId: String): ViewModel() {

    val duration = MutableLiveData<String>()
    val commentThreadList: LiveData<PagedList<ItemResponse<CommentThreadSnippetResponse>>>

    private val api: YouTubeApi = kodein.instance()

    init {
        val factory = object : DataSource.Factory<String, ItemResponse<CommentThreadSnippetResponse>>() {
            override fun create() = CommentThreadDataSource(kodein, videoId)
        }
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setInitialLoadSizeHint(5)
            .setPageSize(5)
            .build()
        commentThreadList = LivePagedListBuilder(factory, config).build()

        GlobalScope.launch {
            // Video duration requires a whole new API call to get the video info.
            val response = api.videoDetails(videoId).execute()
            if (response.isSuccessful && response.body() != null) {
                val durationFormat = response.body()!!.items.first().snippet.duration
                val period = ISOPeriodFormat.standard().parsePeriod(durationFormat)
                val formatter = PeriodFormatterBuilder()
                    .printZeroNever()
                    .appendHours()
                    .appendSuffix("h ")
                    .appendMinutes()
                    .appendSuffix("m ")
                    .appendSeconds()
                    .appendSuffix("s")
                    .toFormatter()
                duration.postValue(period.toString(formatter))
            }
        }
    }

}