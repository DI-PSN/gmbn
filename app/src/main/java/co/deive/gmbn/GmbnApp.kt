package co.deive.gmbn

import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.android.androidModule
import com.github.salomonbrys.kodein.lazy
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration


/**
 * GMBN Application, configures Kodein DI.
 */
class GmbnApp : Application(), KodeinAware {

    override val kodein by Kodein.lazy {
        import(androidModule)
        import(APIModule().module)
    }

    override fun onCreate() {
        super.onCreate()
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this))
    }

}

/**
 * Basic activity that just contains a fragment.
 */
abstract class GmbnActivity : AppCompatActivity() {

    companion object {
        internal const val fragmentTag = "ListFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportFragmentManager.findFragmentByTag(fragmentTag) == null) {
            supportFragmentManager.beginTransaction()
                .add(android.R.id.content, createFragment(), fragmentTag)
                .commit()
        }
    }

    abstract fun createFragment(): Fragment
}

/**
 * ViewModel val property delegate for a model with it's own factory, passes the applications Kodein instance.
 * <code>private val viewModel: ViewModel by viewModelProvider { ViewModel(it) }</code>
 */
@Suppress("UNCHECKED_CAST")
inline fun <reified VM : ViewModel> Fragment.viewModelProvider(
    crossinline provider: (Kodein) -> VM) = lazy {
    ViewModelProviders.of(this, object : ViewModelProvider.Factory {
        override fun <T1 : ViewModel> create(aClass: Class<T1>) =
            provider((activity!!.application as GmbnApp).kodein) as T1
    }).get(VM::class.java)
}

/**
 * Returns the thumbnail with the key for this device size.
 */
fun PlaylistSnippetResponse.getThumbnail(context: Context): ThumbnailResponse {
    return getThumbnail(context, this.thumbnails)
}

/**
 * Returns the thumbnail with the key for this device size.
 */
fun PlaylistItemSnippetResponse.getThumbnail(context: Context): ThumbnailResponse {
    return getThumbnail(context, this.thumbnails)
}

private fun getThumbnail(context: Context, thumbnails: Map<String, ThumbnailResponse>): ThumbnailResponse {
    val thumbnailKey = context.getString(R.string.thumbnail_key)
    return thumbnails[thumbnailKey] ?: thumbnails.values.first()
}