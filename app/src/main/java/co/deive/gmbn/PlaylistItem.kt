package co.deive.gmbn

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.paging.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener
import kotlinx.android.synthetic.main.playlist_item_fragment.*
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.sdk27.coroutines.onClick

/**
 * Just contains a PlaylistItemFragment
 */
class PlaylistItemActivity : GmbnActivity() {

    companion object {
        internal const val ExtraPlaylist = "PlaylistItemActivity.Playlist"

        fun createIntent(context: Context, playlist: ItemResponse<PlaylistSnippetResponse>): Intent {
            val intent = Intent(context, PlaylistItemActivity::class.java)
            intent.putExtra(ExtraPlaylist, playlist)
            return intent
        }
    }

    private val playlist by lazy { intent.getParcelableExtra<ItemResponse<PlaylistSnippetResponse>>(ExtraPlaylist) }

    override fun createFragment(): Fragment {
        val frag = PlaylistItemFragment()
        val args = Bundle()
        args.putParcelable(ExtraPlaylist, playlist)
        frag.arguments = args
        return frag
    }
}

/**
 * Shows a list of playlist items from YouTube.
 */
class PlaylistItemFragment : Fragment() {

    private val playlist by lazy { arguments!!.getParcelable<ItemResponse<PlaylistSnippetResponse>>(PlaylistItemActivity.ExtraPlaylist)!! }
    private val viewModel: PlaylistItemViewModel by viewModelProvider { PlaylistItemViewModel(it, playlist.id) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.playlist_item_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        ViewCompat.setTransitionName(playlist_header, PlaylistActivity.HeroId)
        val imageOptions = DisplayImageOptions.Builder().build()
        ImageLoader.getInstance()
            .displayImage(playlist.snippet.getThumbnail(view.context).url, playlist_header, imageOptions, SimpleImageLoadingListener())

        val adapter = PlaylistItemAdapter { activity }
        playlist_items.adapter = adapter
        viewModel.videoList.observe(this, Observer {
            adapter.submitList(it)
        })
    }

}

/**
 * The item view for a single playlist item.
 */
class PlaylistItemViewHolder(parent: ViewGroup, getActivity: () -> Activity?) : RecyclerView.ViewHolder(parent.context.layoutInflater.inflate(R.layout.playlist_item_item, parent, false)) {

    private val itemTitle: TextView by lazy { itemView.findViewById<TextView>(R.id.playlist_title)  }
    private val itemThumbnail: ImageView by lazy { itemView.findViewById<ImageView>(R.id.playlist_thumbnail)  }

    private var item: ItemResponse<PlaylistItemSnippetResponse>? = null

    init {
        itemView.onClick { item?.let {
            getActivity()?.let { activity ->
                ViewCompat.setTransitionName(itemThumbnail, PlaylistActivity.HeroId)
                val options = ActivityOptionsCompat
                    .makeSceneTransitionAnimation(activity, itemThumbnail, PlaylistActivity.HeroId)
                    .toBundle()
                itemView.context.startActivity(VideoActivity.createIntent(itemView.context, it), options)
            }
        }}
    }

    fun bind(item: ItemResponse<PlaylistItemSnippetResponse>) {
        this.item = item
        itemTitle.text = item.snippet.title
        val imageOptions = DisplayImageOptions.Builder()
            .showImageOnLoading(android.R.color.transparent)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .build()
        ImageLoader.getInstance()
            .displayImage(item.snippet.getThumbnail(itemView.context).url, itemThumbnail, imageOptions, SimpleImageLoadingListener())
    }

}

/**
 * Paged adapter for playlist items.
 */
class PlaylistItemAdapter(private val getActivity: () -> Activity?) : PagedListAdapter<ItemResponse<PlaylistItemSnippetResponse>, PlaylistItemViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PlaylistItemViewHolder(parent, getActivity)

    override fun onBindViewHolder(holder: PlaylistItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    companion object {
        private val diffCallback: DiffUtil.ItemCallback<ItemResponse<PlaylistItemSnippetResponse>> = object : DiffUtil.ItemCallback<ItemResponse<PlaylistItemSnippetResponse>>() {
            override fun areItemsTheSame(old: ItemResponse<PlaylistItemSnippetResponse>, new: ItemResponse<PlaylistItemSnippetResponse>): Boolean {
                return old.id === new.id
            }

            override fun areContentsTheSame(old: ItemResponse<PlaylistItemSnippetResponse>, new: ItemResponse<PlaylistItemSnippetResponse>): Boolean {
                return old == new
            }
        }
    }
}

/**
 * Paged data source loading playlist pages fom YouTube.
 */
class PlaylistItemDataSource(kodein: Kodein, private val playlistId: String) : PageKeyedDataSource<String, ItemResponse<PlaylistItemSnippetResponse>>() {

    private val api: YouTubeApi = kodein.instance()

    override fun loadInitial(
        params: LoadInitialParams<String>,
        callback: LoadInitialCallback<String, ItemResponse<PlaylistItemSnippetResponse>>) {
        val playlistResponse = api.itemsForPlaylist(playlistId, maxResults = params.requestedLoadSize).execute()
        if (playlistResponse.isSuccessful && playlistResponse.body() != null) {
            val playlist = playlistResponse.body()!!
            callback.onResult(playlist.items, 0, playlist.pageInfo.totalResults,
                playlist.prevPageToken, playlist.nextPageToken)
        }
        else {
            callback.onResult(emptyList(), 0, 0, null, null)
        }
    }

    override fun loadAfter(
        params: LoadParams<String>,
        callback: LoadCallback<String, ItemResponse<PlaylistItemSnippetResponse>>) {
        val playlistResponse = api.itemsForPlaylist(playlistId, pageToken = params.key, maxResults = params.requestedLoadSize).execute()
        if (playlistResponse.isSuccessful && playlistResponse.body() != null) {
            val playlist = playlistResponse.body()!!
            callback.onResult(playlist.items, playlist.nextPageToken)
        }
        else {
            callback.onResult(emptyList(), null)
        }
    }

    override fun loadBefore(
        params: LoadParams<String>,
        callback: LoadCallback<String, ItemResponse<PlaylistItemSnippetResponse>>) {
        val playlistResponse = api.itemsForPlaylist(playlistId, pageToken = params.key, maxResults = params.requestedLoadSize).execute()
        if (playlistResponse.isSuccessful && playlistResponse.body() != null) {
            val playlist = playlistResponse.body()!!
            callback.onResult(playlist.items, playlist.prevPageToken)
        }
        else {
            callback.onResult(emptyList(), null)
        }
    }

}

/**
 * Playlist view model to hold playlist data.
 */
class PlaylistItemViewModel(kodein: Kodein, playlistId: String): ViewModel() {

    val videoList: LiveData<PagedList<ItemResponse<PlaylistItemSnippetResponse>>>

    init {
        val factory = object : DataSource.Factory<String, ItemResponse<PlaylistItemSnippetResponse>>() {
            override fun create() = PlaylistItemDataSource(kodein, playlistId)
        }
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setInitialLoadSizeHint(5)
            .setPageSize(5)
            .build()
        videoList = LivePagedListBuilder(factory, config).build()
    }

}