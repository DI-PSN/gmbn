package co.deive.gmbn

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.paging.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener
import kotlinx.android.synthetic.main.playlist_fragment.*
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.sdk27.coroutines.onClick

/**
 * Just contains a PlaylistFragment
 */
class PlaylistActivity : GmbnActivity() {

    companion object {
        const val HeroId = "Playlist.Hero"
    }

    override fun createFragment() = PlaylistFragment()
}

/**
 * Shows a list of playlist items from YouTube.
 */
class PlaylistFragment : Fragment() {

    private val viewModel: PlaylistViewModel by viewModelProvider { PlaylistViewModel(it) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.playlist_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = PlaylistAdapter { activity }
        playlist.adapter = adapter
        viewModel.videoList.observe(this, Observer {
            adapter.submitList(it)
        })
    }

}

/**
 * The item view for a single playlist.
 */
class PlaylistViewHolder(parent: ViewGroup, getActivity: () -> Activity?) : RecyclerView.ViewHolder(parent.context.layoutInflater.inflate(R.layout.playlist_item, parent, false)) {

    private val itemTitle: TextView by lazy { itemView.findViewById<TextView>(R.id.playlist_title)  }
    private val itemThumbnail: ImageView by lazy { itemView.findViewById<ImageView>(R.id.playlist_thumbnail)  }

    private var item: ItemResponse<PlaylistSnippetResponse>? = null

    init {
        itemView.onClick { item?.let {
            getActivity()?.let { activity ->
                ViewCompat.setTransitionName(itemThumbnail, PlaylistActivity.HeroId)
                val options = ActivityOptionsCompat
                    .makeSceneTransitionAnimation(activity, itemThumbnail, PlaylistActivity.HeroId)
                    .toBundle()
                itemView.context.startActivity(PlaylistItemActivity.createIntent(itemView.context, it), options)
            }
        }}
    }

    fun bind(item: ItemResponse<PlaylistSnippetResponse>) {
        this.item = item
        itemTitle.text = item.snippet.title
        val imageOptions = DisplayImageOptions.Builder()
            .showImageOnLoading(android.R.color.transparent)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .build()
        ImageLoader.getInstance()
            .displayImage(item.snippet.getThumbnail(itemView.context).url, itemThumbnail, imageOptions, SimpleImageLoadingListener())
    }

}

/**
 * Paged adapter for playlist items.
 */
class PlaylistAdapter(private val getActivity: () -> Activity?) : PagedListAdapter<ItemResponse<PlaylistSnippetResponse>, PlaylistViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PlaylistViewHolder(parent, getActivity)

    override fun onBindViewHolder(holder: PlaylistViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    companion object {
        private val diffCallback: DiffUtil.ItemCallback<ItemResponse<PlaylistSnippetResponse>> = object : DiffUtil.ItemCallback<ItemResponse<PlaylistSnippetResponse>>() {
            override fun areItemsTheSame(old: ItemResponse<PlaylistSnippetResponse>, new: ItemResponse<PlaylistSnippetResponse>): Boolean {
                return old.id === new.id
            }

            override fun areContentsTheSame(old: ItemResponse<PlaylistSnippetResponse>, new: ItemResponse<PlaylistSnippetResponse>): Boolean {
                return old == new
            }
        }
    }
}

/**
 * Paged data source loading playlist pages fom YouTube.
 */
class PlaylistDataSource(kodein: Kodein) : PageKeyedDataSource<String, ItemResponse<PlaylistSnippetResponse>>() {

    private val api: YouTubeApi = kodein.instance()

    override fun loadInitial(
        params: LoadInitialParams<String>,
        callback: LoadInitialCallback<String, ItemResponse<PlaylistSnippetResponse>>) {
        val playlistResponse = api.playlistForChannel(maxResults = params.requestedLoadSize).execute()
        if (playlistResponse.isSuccessful && playlistResponse.body() != null) {
            val playlist = playlistResponse.body()!!
            callback.onResult(playlist.items, 0, playlist.pageInfo.totalResults,
                playlist.prevPageToken, playlist.nextPageToken)
        }
        else {
            callback.onResult(emptyList(), 0, 0, null, null)
        }
    }

    override fun loadAfter(
        params: LoadParams<String>,
        callback: LoadCallback<String, ItemResponse<PlaylistSnippetResponse>>) {
        val playlistResponse = api.playlistForChannel(pageToken = params.key, maxResults = params.requestedLoadSize).execute()
        if (playlistResponse.isSuccessful && playlistResponse.body() != null) {
            val playlist = playlistResponse.body()!!
            callback.onResult(playlist.items, playlist.nextPageToken)
        }
        else {
            callback.onResult(emptyList(), null)
        }
    }

    override fun loadBefore(
        params: LoadParams<String>,
        callback: LoadCallback<String, ItemResponse<PlaylistSnippetResponse>>) {
        val playlistResponse = api.playlistForChannel(pageToken = params.key, maxResults = params.requestedLoadSize).execute()
        if (playlistResponse.isSuccessful && playlistResponse.body() != null) {
            val playlist = playlistResponse.body()!!
            callback.onResult(playlist.items, playlist.prevPageToken)
        }
        else {
            callback.onResult(emptyList(), null)
        }
    }

}

/**
 * Playlist view model to hold playlist data.
 */
class PlaylistViewModel(kodein: Kodein): ViewModel() {

    val videoList: LiveData<PagedList<ItemResponse<PlaylistSnippetResponse>>>

    init {
        val factory = object : DataSource.Factory<String, ItemResponse<PlaylistSnippetResponse>>() {
            override fun create() = PlaylistDataSource(kodein)
        }
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setInitialLoadSizeHint(5)
            .setPageSize(5)
            .build()
        videoList = LivePagedListBuilder(factory, config).build()
    }

}